```plantuml

@startuml Abstract factory with Proxy


class IMessageFactory{
  IMessageFactory()
  CreateMessage() : IMessage
  CreateEmoji() : Emoji
}

class EmailMessageFactory{
  EmailMessageFactory()
  CreateMessage() : IMessage
  CreateEmoji() : Emoji
}

class SmsMessageFactory {
  EmailMessageFactory()
  CreateMessage() : IMessage
  CreateEmoji() : Emoji
}

class IMessage{
  Send()
}

class Emoji{
  Send()
}

class EmailMessage{
  EmailMessage()
}

class SmsMessage{
  SMSMessage()
}

class EmailEmoji{
  EmailEmoji()
}

class SMSEmoji{
  SMSEmoji()
}
class Client{
  - imessage : IMessage
  - emoji: Emoji
  Client()
}

class MessageProxy {
  - IMessage _realMessage
  - Emoji _realEmoji
  MessageProxy()
  Send()
  CheckAccess()
  LogAccess()
}

Client --> IMessageFactory
Client --> IMessage
Client --> MessageProxy
Client --> Emoji

MessageProxy --> IMessage
MessageProxy --> Emoji

EmailMessageFactory --> EmailMessage
SmsMessageFactory --> SmsMessage
EmailMessageFactory --> EmailEmoji
SmsMessageFactory --> SMSEmoji

IMessageFactory <-- EmailMessageFactory 
IMessageFactory <-- SmsMessageFactory 

IMessage <-- EmailMessage
IMessage <-- SmsMessage
Emoji <-- EmailEmoji
Emoji <-- SMSEmoji
@enduml

```
