using System;

namespace MessagingSystem
{
    // Abstract Factory
    public interface IMessageFactory
    {
        IMessage CreateMessage();
        IEmoji CreateEmoji();
    }

    // Concrete Factory for Email Messages
    public class EmailMessageFactory : IMessageFactory
    {
        public IMessage CreateMessage()
        {
            return new MessageProxy(new EmailMessage());
        }
        public IEmoji CreateEmoji()
        {
            return new EmojiProxy(new EmailEmoji());
        }
    }

    // Concrete Factory for SMS Messages
    public class SmsMessageFactory : IMessageFactory
    {
        public IMessage CreateMessage()
        {
            return new MessageProxy(new SmsMessage());
        }
        public IEmoji CreateEmoji()
        {
            return new EmojiProxy(new EmailEmoji());
        }
    }

    // Abstract Product
    public interface IMessage
    {
        void Send();
    }

    public interface IEmoji
    {
        void Send();
    }
    // Concrete Product: Email Message
    public class EmailMessage : IMessage
    {
        public void Send()
        {
            Console.WriteLine("Sending email message...");
        }
    }

    // Concrete Product: SMS Message
    public class SmsMessage : IMessage
    {
        public void Send()
        {
            Console.WriteLine("Sending SMS message...");
        }
    }

    public class EmailEmoji : IEmoji
    {
        public void Send()
        {
            Console.WriteLine("Sending emoji message...");
        }
    }


    public class SmsEmojie : IEmoji
    {
        public void Send()
        {
            Console.WriteLine("Sending emoji message...");
        }
    }
    // Proxy
    public class MessageProxy : IMessage
    {
        private IMessage _realMessage;

        public MessageProxy(IMessage realMessage)
        {
            this._realMessage = realMessage;
        }

        public void Send()
        {
            if (this.CheckAccess())
            {
                this._realMessage.Send();

                this.LogAccess();
            }
        }

        public bool CheckAccess()
        {
            Console.WriteLine("Proxy: Checking user's access before sending a message...");

            // Perform access control logic here

            return true;
        }

        public void LogAccess()
        {
            Console.WriteLine("Proxy: Logging the message sending operation.");
        }
    }
    public class EmojiProxy : IEmoji
    {
        private IEmoji _realEmoji;

        public EmojiProxy(IEmoji realEmoji)
        {
            this._realEmoji = realEmoji;
        }

        public void Send()
        {
            if (this.CheckAccess())
            {
                this._realEmoji.Send();

                this.LogAccess();
            }
        }

        public bool CheckAccess()
        {
            Console.WriteLine("Proxy: Checking user's access before sending an emoji...");

            // Perform access control logic here

            return true;
        }

        public void LogAccess()
        {
            Console.WriteLine("Proxy: Logging the emoji sending operation.");
        }
    }

    public class Client
    {
        private IMessage imessage;
        private IEmoji iemoji;

        public void SendMessage(IMessageFactory factory)
        {
            imessage = factory.CreateMessage();
            imessage.Send();

        }
        public void SendEmoji(IMessageFactory factory)
        {
            iemoji = factory.CreateEmoji();
            iemoji.Send();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Client client = new Client();

            Console.WriteLine("Client: Sending an email message (EmailMessageFactory):");
            client.SendMessage(new EmailMessageFactory());

            Console.WriteLine();

            Console.WriteLine("Client: Sending an SMS message (SmsMessageFactory):");
            client.SendMessage(new SmsMessageFactory());

            Console.WriteLine();

            Console.WriteLine("Client: Sending an Emoji message (SmsMessageFactory):");
            client.SendEmoji(new SmsMessageFactory());
            Console.WriteLine();

            Console.WriteLine("Client: Sending an Emoji message (SmsMessageFactory):");
            client.SendEmoji(new SmsMessageFactory());
        }
    }
}
